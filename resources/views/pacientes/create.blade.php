@extends('layout')

@section('content')

    {!! Form::open(['route' => 'pacientes.store', 'method' => 'POST']) !!}
        @include('pacientes.partials.fields')
        <button type="submit" class="btn btn-default">Crear</button>
    {!! Form::close() !!}


@endsection