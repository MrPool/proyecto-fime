<table class="table table-striped">
    <tr>
        <th>#</th>
        <th>CURP</th>
        <th>Primer Apellido</th>
        <th>Segundo Apellido</th>
        <th>Nombre</th>
        <th>FecNac</th>
        <th>EdoNac</th>
        <th>Sexo</th>
        <th>NacOrigen</th>
        <th>Folio</th>
        <th>Estado</th>
        <th>Municipio</th>
        <th>Localidad</th>
        <th>Acciones</th>
    </tr>
    @foreach($pacientes as $paciente)
        <tr data-id="{{$paciente -> id}}">
            <td>{{$paciente -> id}}</td>
            <td>{{$paciente -> CURP}}</td>
            <td>{{$paciente -> primer_apellido}}</td>
            <td>{{$paciente -> segundo_apellido}}</td>
            <td>{{$paciente -> nombre}}</td>
            <td>{{$paciente -> fecnac}}</td>
            <td>{{$paciente -> edonac}}</td>
            <td>{{$paciente -> sexo}}</td>
            <td>{{$paciente -> nacorigen}}</td>
            <td>{{$paciente -> folio}}</td>
            <td>{{$paciente -> edo}}</td>
            <td>{{$paciente -> mun}}</td>
            <td>{{$paciente -> loc}}</td>
            <td>
                <a href="{{ route('pacientes.edit', $paciente) }}">Editar</a>
                <a href="" class="btn-delete">Eliminar</a>
            </td>
        </tr>
    @endforeach
</table>
