<div class="form-group">
    {!! Form::label('CURP', 'CURP') !!}
    {!!  Form::text('CURP', null, ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su CURP']) !!}
</div>
<div class="form-group">
    {!! Form::label('primer_apellido', 'Primer Apellido') !!}
    {!!  Form::text('primer_apellido', null, ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su primer apellido']) !!}
</div>
<div class="form-group">
    {!! Form::label('segundo_apellido', 'Segundo Apellido') !!}
    {!!  Form::text('segundo_apellido', null,  ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su segundo apellido']) !!}
</div>
<div class="form-group">
    {!! Form::label('nombre', 'Nombre') !!}
    {!!  Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su nombre']) !!}
</div>
<div class="form-group">
    {!! Form::label('fecnac', 'Fecha de Nacimiento') !!}
    {!!  Form::text('fecnac', null, ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su fecha de naciemto (aaaammdd)']) !!}
</div>
<div class="form-group">
    {!! Form::label('edonac', 'Estado de nacimiento') !!}
    {!!  Form::text('edonac', null, ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su estado de nacimiento']) !!}
</div>
<div class="form-group">
    {!! Form::label('nacorigen', 'Nacionalidad') !!}
    {!!  Form::text('nacorigen', null, ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su nacionalidad']) !!}
</div>
<div class="form-group">
    {!! Form::label('folio', 'Folio') !!}
    {!!  Form::text('folio', null, ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su folio']) !!}
</div>
<div class="form-group">
    {!! Form::label('edo', 'Estado') !!}
    {!!  Form::text('edo', null, ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su estado']) !!}
</div>
<div class="form-group">
    {!! Form::label('mun', 'Municipio') !!}
    {!!  Form::text('mun', null, ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su municipio']) !!}
</div>
<div class="form-group">
    {!! Form::label('loc', 'Localidad') !!}
    {!!  Form::text('loc', null, ['class' => 'form-control', 'placeholder' => 'Por favor introduzca su localidad']) !!}
</div>
<div class="form-group">
    {!! Form::label('sexo', 'Sexo') !!}
    {!!  Form::select('sexo', ['' => 'Seleccione tipo', 'M' => 'M', 'H' => 'H'], null, ['class' => 'form-control']) !!}
</div>

