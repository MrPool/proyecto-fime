@extends('layout')

@section('content')
    <h1>Pacientes en el sistema</h1>
    <p>Hay {{ $pacientes->total() }} pacientes.</p>
    <p>
        <a class="btn btn-info" href="{{ route('pacientes.create') }}" role="button">
            Crear Usuario
        </a>
    </p>
    @include('pacientes.partials.table')
    {{ $pacientes -> render() }}

    {!! Form::open(['route' => ['pacientes.destroy', 'USER_ID'], 'method' => 'DELETE', 'id' => 'form-delete']) !!}
    {!! Form::close() !!}
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('.btn-delete').click(function(e) {

                e.preventDefault();

                var row = $(this).parents('tr');
                var id = row.data('id');
                var form = $('#form-delete');
                var url = form.attr('action').replace('USER_ID', id);
                var data = form.serialize();

                row.fadeOut();

                $.post(url, data, function(result) {
                    alert(result);
                });
            });
        });

    </script>
@endsection
