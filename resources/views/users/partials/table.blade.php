<table class="table table-striped">
    <tr>
        <th>#</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Email</th>
        <th>Categoría</th>
        <th>Permisos</th>
        <th>Acciones</th>
    </tr>
    @foreach($users as $user)
        <tr data-id="{{$user -> id}}">
            <td>{{$user -> id}}</td>
            <td>{{$user -> first_name}}</td>
            <td>{{$user -> last_name}}</td>
            <td>{{$user -> email}}</td>
            <td>{{$user -> categoria}}</td>
            <td>{{$user -> permisos}}</td>
            <td>
                <a href="{{ route('users.edit', $user->id) }}">Editar</a>
                <a href="" class="btn-delete">Eliminar</a>
            </td>
        </tr>
    @endforeach
</table>
