<?php

namespace App;

use Illuminate\Foundation\Auth\Paciente as Authenticatable;

class Paciente extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'CURP', 'primer_apellido', 'segundo_apellido', 'nombre',  'fecnac',  'edonac', 'sexo', 'nacorigen', 'folio', 'edo', 'mun', 'loc',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}

