<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 6/05/16
 * Time: 02:17 AM
 */

use Illuminate\Database\Seeder;
use  Faker\Factory as Faker;

class PacientesTableSeeder extends Seeder {

    public function run()
    {

        $faker = Faker::create();

        for ($i = 1; $i < 100; $i++) {
            \DB::table('pacientes')->insert(array(
                'curp' => $faker->unique()->regexify('[a-z0-9]{18}'),
                'primer_apellido' => $faker->lastName,
                'segundo_apellido' => $faker->lastName,
                'nombre' => $faker->firstName,
                'fecnac' => $faker->date($format = 'Ymd', $max = 'now'),
                'edonac' => $faker->numberBetween($min = 1, $max = 24),
                'sexo' => 'M',
                'nacorigen' => $faker->numberBetween($min = 1, $max = 684),
                'folio' => $faker->unique()->regexify('[a-z0-9]{18}'),
                'edo' => $faker->numberBetween($min = 1, $max = 32),
                'mun' => $faker->numberBetween($min = 1, $max = 570),
                'loc' => $faker->numberBetween($min = 1, $max = 8542)

            ));
        }
    }
}