<?php
/**
 * Created by PhpStorm.
 * User: pedro
 * Date: 6/05/16
 * Time: 02:17 AM
 */

use Illuminate\Database\Seeder;
use  Faker\Factory as Faker;

class UserTableSeeder extends Seeder {

    public function run()
    {

        $faker = Faker::create();

        for ($i = 0; $i < 30; $i++) {
            \DB::table('users')->insert(array(
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => $faker->unique()->email,
                'password' => \Hash::make('123456'),
                'categoria' => '2',
                'permisos' => '2'

            ));
        }
    }
}